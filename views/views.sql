--ejemplo 1:este ejemplo mustra todo las personas que tienes libro
create view compradores
as
SELECT Persona.nombre, Persona.apellido, Persona.genero, Persona.persona_id, Libro.titulo, Libro.precio
FROM            dbo.Persona INNER JOIN
                         dbo.Libro ON dbo.Persona.persona_id = dbo.Libro.persona_id

select *
from compradores

--ejemplo 2:muestra el detalle de compras realizadas

create view detalle_de_compras
as
SELECT compra.id_compra, compra.ID_PRODUCTO, compra.FECHA_COMPRA, compra.CANTIDAD_COMPRA,
 proveedor.id_proveedor, proveedor.nombre_proveedor,
proveedor.nacionalidad_proveedor, proveedor.mail1, compra.PRECIO_COMPRA 
FROM compra INNER JOIN proveedor ON dbo.compra.id_proveedor = dbo.proveedor.id_proveedor

select * from detalle_de_compras

-- ejemplo 3: este ejemplo muestra el detalle de ventas realizadas
create view ventas_realizadas
as
SELECT cliente.id_cliente, cliente.nit, cliente.razon_social, cliente.nacionalidad_cliente, cliente.ciudad_cliente,
 cliente.telefono_fijo1, cliente.nombre, cliente.mail1, venta.id_venta, venta.ID_PRODUCTO, venta.fecha_venta,
 venta.cantidad_venta, venta.precio_venta, venta.descuento, venta.detalle_venta
FROM cliente INNER JOIN venta ON cliente.id_cliente = venta.id_cliente

select * from ventas_realizadas