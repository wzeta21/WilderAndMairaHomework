Cuarta tarea
-----------------------
Integrantes:

Nombre : "Wilder Zapata Miranda".

Entranando para: "Mobile development".

Nombre : "Maira Huarachi".

Entrenando para: "QA"

VIEWS (Vistas):
--------------
Una vista es una tabla virtual, las vistas se contruyen a partir de de consultas elaboradas, las vistas facilitan
el trabajo de realizar un consulta. Una vista se comporta como una tabla se puede ralizar consulatas a la vista cual si fuera una tabla, las vistas no se crean fisicamente en el gestor de base de datos, son solo representacion de una consulta.

PROCEDIMIENTOS ALMACENADOS (STORED PROCEDURE):
----------------------------------------------
Los procedimientos almacenados son grupos formados por instrucciones SQL. Cuando se ejecuta un procedimiento almacenado,se prepara un plan de ejecución para que la ejecución sea muy rápida.

La posibilidad de escribir procedimientos almacenados mejora notablemente la potencia, eficacia y flexibilidad de SQL.

Los procedimientos almacenados pueden:
- Incluir parámetros
- Llamar a otros procedimientos
- Devolver un valor de estado a un procedimiento de llamada o lote para indicar eléxito o el fracaso del mismo y la razón de dicho fallo
- Devolver valores de parámetros a un procedimiento de llamada o lote
- Ejecutarse en SQL Server remotos