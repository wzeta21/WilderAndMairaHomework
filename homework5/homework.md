## 1. ¿Es necesario colocar NOT NULL cuando se usa PRIMARY KEY?
La palabra reservada NOT NULL no es necesario colocar cuando:
- Se establece la lave primaria al momento de crear la tabla

Ejemplo:
```
create table Autor(
    id int,
    nombre varchar(10)
    primary key(id)
);
```
o
```
create table Autor(
    id int primary key,
    nombre varchar(10)
);
```
La palabra reservada NOT NULL es necesario colocar cuando:
- Se desea establecer lave primaria despues de la creacion de la tabla, en este caso si es necesario colocar NOT NULL al atributo que va a ser llave primaria, si no se establece NOT NULL no se podrá establecer como llave primaria al atributo, para solucionar esto habrá que modificar al atributo.

ejemplo correcto:
```
create table Autor(
    id int NOT NULL,
    nombre varchar(10)
);
alter table Autor add constraint llave primary key(id)
```

ejemplo incorrecto:
```
create table Autor(
    id int,
    nombre varchar(10)
    primary key(id)
);
alter table Autor add constraint llave primary key(id)
```

## 2. ¿Cuando es recomendable usar  el tipo de dato  MONEY  y  DECIMAL?
Para ver cuando es recomendable usar MONEY y DECIMAL se puede citar los siguientes:
- **Favorecer a tipos de datos nativo (Native Data Type)**, usa el tipo de datos nativo no necesita una sobrecarga innecesaria de los recursos, por lo tanto, es más pequeño y rápido. MONEY (Native Data Type) necesita 8 Byte, NUMERICAL y DECIMAL (19,4) necesita 9 bytes (12,5% más grande). El tipo MONEY es más rápido, siempre que se use para significar (como dinero), ¿qué tan rápido? El procesamiento de tipo de dato MONEY es 2 veces más rápido que manipular DECIMAL y NUMERIC
- **Mejor manejar MONEY**, MONEY mejor para almacenar MONEY y hacer operaciones, por ejemplo, en contabilidad, informe único puede ejecutar millones de SUMAS y pocas multiplicaciones después de la operación suma realizada, para una aplicación contable muy grande casi dos veces más rápido es extremadamente significativo.
- **Baja precisión del tipo MONEY**, el dinero en la vida real no tiene que se muy preciso, esto ser refiere a que a muchas personas no les importar aproximadamente 1 centavo $, pero ¿qué hay de 0,01 centavos de dólar?, si el TIPO MONEY pierde esos centavos en muchas operaciones, esos centavos significan mucho en este caso se debería usar DECIMAL o NUMERIC para no perder esos centavos
- **El tipo MONEY** es de precisión limitada solo tiene una precisión de 4 dígitos (después de la coma), por lo que debe convertirse a otro tipo antes de realizar una operación como división, pero de nuevo se deber reconvertir  resultado de una operación al tipo MONEY, esto significa gasto de recursos del servidor.


Entonces como conclusión se puede decir usara tipo MONEY si no se manipula esos datos  con operaciones esto es si solo guarda valores de dinero, pero si necesita precisión y no quiere perder valores es recomendable usar NUMERIC o DEDCIMAL

## 3. ¿Hay alguna diferencia entre los tipos de datos DECIMAL  y  NUMERIC?

Son sinónimos, no hay diferencia. Los tipos de datos DECIMAL y NUMERIC son tipos de datos numéricos con precisión y escala fijas.
En lo que difieren es que uno precede a otro, NUMERIC precede a DECIMAL, cuando se realizan operaciones aritméticas entre los dos tipos de datos el resultante siempre será un NUMERIC ahí es cuando se refleja la **precedencia de tipo de datos**.

## 4. Investigar sobre  FUNCTIONS  en  SQL server y desarrollar 3 ejemplos.

**Funciones con SQL Server**
------------------------
Una función es una rutina almacenada que recibe parámetros escalares de entrada, los procesa según la definición de la función y finalmente retorna en un resultado de un tipo específico que permitirá su utilización con un objetivo.

Las funciones definidas por el usuario en SQL Server permiten retornar tablas en los resultados. Esta característica proporciona al programador facilidad a la hora de administrar sus bases de datos.

Existen tres tipos de funciones:
- Funciones escalares
- Funciones con valores de tabla en línea
- Funciones con valores de tabla y múltiples instrucciones


La sintaxis de creación de las tres es muy similar, sólo se diferencian en el tipo de parámetros que retornan.

**Funciones escalares**
---------------------
Las funciones escalares son aquellas que reciben parámetros de entrada y al final retornar en un valor con un tipo de dato. Es decir un  tipo de dato como INT, FLOAT, VARCHAR, etc. SQL Server no permite que este tipo de funciones retorne valores de tipo text, ntext, image, cursor y timestamp. Se utiliza la palabra reservada Returns para indicar el tipo de dato en el cual retornará la función. 


Ejemplo de función escalar:Calcular la edad de las perosnas a partir de su fecha de nacimeinto.

```
create function CalculateAge(@Id int) returns smallint
as
begin
    declare @age as int

    select @age = datediff(year, fechaNacimiento, getdate())
    from Persona
    where persona_id = @Id
    return @age
end

select p.nombre, dbo.CalculateAge(10) edad
from Persona p
```

**Funciones con Valores de Tabla en Línea**
---------------------------------------
Este tipo de función tiene la misma sintaxis que una función escalar, la única diferencia es que devuelve tipo de dato TABLE (una tabla compuesta de registros).

Ejemplo: Listar todas las personas de un pais determinado y ademas mostrar sus edades.

```
create function MayoredEdadDeXPais(@pais varchar(20)) returns table
as
return(
    select p.*, dbo.CalculateAge(p.persona_id) edad
    from Persona p
    where dbo.CalculateAge(p.persona_id) >=18 and p.pais = @pais
)

select *
from dbo.MayoredEdadDeXPais('Brasil')
```

**Funciones con Valores de Tabla y Múltiples Instrucciones**
--------------------------------------------------------
Este tipo de funciones es similar a las funciones de tabla en línea, pero ahora incluyen un bloque de sentencias para manipular la información antes de retornar la tabla.

Ejemplo:Mostrar todo las personas de X genero y sean amyores de X edad

```
create function porGeneroYMayores(@genero char, @age int)
returns @MayoresConLibro table(
    nombre varchar(30), 
    edad int,
    libro varchar(30),
    precio_libro int
)
as
begin
    insert @MayoresConLibro
    select p.nombre, dbo.CalculateAge(p.persona_id) edad, l.titulo, l.precio
    from Persona p inner join Libro l on p.persona_id = l.persona_id
    where dbo.CalculateAge(p.persona_id) >= @age and p.genero = @genero
    return
end


select *
from dbo.porGeneroYMayores('f', 30)
```