## INDICES EN SQL
**"Un índice acelera una busqueda"**, es decir, si un campo de una tabla esta indexado entonces el resultado de buscar por ese campo dara una respuesta en un tiempo menor, por el contrario si ese campo no estaria indexado entonces el tiempo de respueata de esa misma busqueda seria mas lento.

Si hablamos del índeces de un libro, éste nos ayuda a dirigirnos directamente a un página específica, pero no cambia la estructura del libro, solo ayuda a encontrar mas rápido lo que se busca, lo propio ocurre con índices en la base de datos, no cambia en absoluto la estuctura de una tabla solo coloca un apuntador a un campo determinado que nos ayuda a obterner en menor tiempo si se busca por ese campo.

Los índices no se usan ni se invocan explícitamente, sino el gestor de base de datos (DBMS) usa a los indices.

Ejemplo:

Se tiene la siguiente tabla:

(Libro)
**:Id:Nombre:Titulo:Precio:IdAutor:**

Crear un índex para el campo titulo:
**create index inx_titulo on Libro(Titulo)**

**select Nombre from Libro**

En esta consulta no se usa el índice, porque no se hace referencia en ningún momento a la campo **Titulo**

**select Titulo, Precio from Libro**

En esta consulta el gestor de base de datos implícitamente hace uso el índice **inx_titulo** poque se hace referncia a al campo **Titulo**.

