## TRIGGER (disparador o desencadenador)

- un trigger se ejecuta cuando se intenta modificar los datos de una tabla (o vista).
- se dispara AUTOMATICAMENTE para eventos (insert, update, delete)
- Se definen para una tabla (o vista) ESPECIFICA.

- Se crean para conservar la integridad referencial y la coherencia entre los datos entre distintas tablas.

un TRIGGER
----------

- no pueden ser invocados directamente
- no reciben y retornan parámetros
- son apropiados para mantener la integridad de los datos,
- no se emplean para obtener resultados de consultas.
- pueden hacer referencia a campos de otras tablas.

Se ejecutan DESPUES de la ejecución de una instrucción "insert", "update" o "delete" en la tabla en la que fueron definidos. Las restricciones se comprueban ANTES de la ejecución de una instrucción "insert", "update" o "delete". Por lo tanto, las restricciones se comprueban primero, si se infringe alguna restricción, el desencadenador no llega a ejecutarse.


Sintaxis básica:
```
 create triggre NOMBREDISPARADOR
  on NOMBRETABLA
  for EVENTO- insert, update o delete
 as 
  SENTENCIAS
```
Analizamos la sintaxis:

Consideraciones generales:
--------------------------

- un disparador se crea solamente en la base de datos actual pero puede hacer referencia a objetos de otra base de datos.

- En un trigger NO SE PERMITEN: create database, alter database, drop database, load database, restore database, load log, reconfigure, restore log, disk init, disk resize.

- Se pueden crear varios triggers para cada evento, es decir
