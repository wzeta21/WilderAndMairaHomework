
-- EJEMPLO 1: inserta una empresa, la tabla empresa no esta establecida su autoincremento, por
-- se control el incremento de los ids en este procedimiento.
USE empresa
GO

CREATE procedure SpInsertarEmpresa
    @Nombre varchar(200),
    @Direccion varchar(200),
    @Telefono int
as
declare @Id int;
--obteniendo el mayor ID de las empresas
select @Id = Max(ID)
from EMPRESA

if ISNUMERIC(@Id) = 0
	set @Id = 1;
else
	set @Id = @Id + 1;

insert into empresa
    (id, nombre, direccion, telefono)
values
    (@Id, @Nombre, @Direccion, @Telefono);

select ID
from empresa
where ID = @Id;

-- EJEMPLO 2: Este ejemplo actualiza una empresa, primero verifica si existe o no el id,
-- si existe el id actualiza sino lanza un error

USE empresa
Go
CREATE procedure SpActualizarEmpresa
    @Id int,
    @Nombre varchar(200),
    @Direccion varchar(200),
    @Telefono int
as
if exists(select *
from empresa
where ID =@Id)
begin
    update empresa set NOMBRE = @Nombre, DIRECCION = @Direccion, TELEFONO = @Telefono
	where ID = @Id
end
else
begin
    raiserror('Id inválido, no existe!', 12, 1)
end

--EJEMPLO 3: este ejemplo solo filtra empresas por ID, es para demostracion solamente , esto se pued realizar
--vista
USE empresa
GO
CREATE procedure SpSeleccionarEmpresasPorId
    @Id int
as
select *
from empresa
where ID = @Id

--ejemplo 4:este ejemplo activa un trabajador que ya estaba registrado pero estaba inactivo

USE trabajadores
GO

CREATE procedure sp_activar_trabajador_asignado
    @IdTrabajador int,
    @IdProyecto int
as
if not exists(select *
from asignacion_trabajadores as asig
where asig.cod_trabajador = @IdTrabajador and asig.CodProy <> @IdProyecto and asig.activo = 'True')
begin
    update asignacion_trabajadores
	set activo = 'True'
	where CodProy = @IdProyecto and cod_trabajador = @IdTrabajador

    declare @cod int;
    select @cod = max(cod_historia)
    from historial_trabajador;
    if ISNUMERIC(@cod) =0
				set @cod = 1;
			else
				set @cod = @cod +1;
    insert into historial_trabajador
        (cod_historia, cod_trabajador, CodProy, accion, fecha)
    values(@cod, @IdTrabajador, @IdProyecto, 'activacion', GETDATE());
end
else
	raiserror('El trabaja que intententa activar esta trabajando en OTRO proyecto', 13, 1);

--EJEMPLO 5: Este ejemplo solo actualiza un matirialDB
USE Trabajadores

CREATE PROCEDURE sp_editar_material
@CodMat int,
@CodProy int,
@FechCompMat datetime,
@NomMat varchar(50),
@UniMedMat varchar(50),
@PreciUnidMat decimal(18,2),
@CantMat int,
@PreciTotMat decimal(18,2),
@ObsMat varchar(50),
@DarBajaProy bit
AS

UPDATE MaterialBD
SET CodMat = @CodMat, CodProy = @CodProy, FechCompMat = @FechCompMat, 
NomMat = @NomMat, UniMedMat = @UniMedMat, PreciUnidMat = @PreciUnidMat, 
CantMat = @CantMat, PreciTotMat = @PreciTotMat, ObsMat = @ObsMat, 
DarBajaProy = @DarBajaProy
WHERE CodMat = @CodMat