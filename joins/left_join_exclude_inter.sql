--LEFT JOIN excluyendo la intersección: este tipo de join devuelve solo las tuplas de la tabla 
--de la izquierda pero que tenga relacion con la otra tabla
--ejemplo:mustra todas las personas que no tienen libros comprados
select p.persona_id, p.nombre, p.apellido, l.titulo, l.precio
from Persona p left outer join Libro l on p.persona_id = l.persona_id
where l.titulo is null

--ejemplo 2: Muestra los libros sin dueño 
select l.*
from Persona p left outer join Libro l on p.persona_id = l.persona_id
where l.libro_id is null
