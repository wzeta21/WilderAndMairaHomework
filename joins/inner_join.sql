--INER JOIN: 
--El resultado de hacer un inner join devulve todo los datos comunes entre las dotas tablas
-- involucradas,  es decir solo devuelve las tuplas que tienen relación entre las dos tablas involucradas en el join.
--ejemplo1:todas las personas que compraron libros 
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p inner join Libro l on p.persona_id = l.persona_id

--ejemplo2: muestra todos los clientes que realizaron algun pedido ordenados por nombre
use Tienda
SELECT Cliente.NonmbreCliente, Pedido.Pedido_id 
FROM Cliente INNER JOIN Pedido ON Cliente.Cliente_id=Pedido.Cliente_id
ORDER BY Cliente.NonmbreCliente;

