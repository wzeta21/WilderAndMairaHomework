--SEMI JOIN: 
--semi join es paresido a un join pero con la partiularidad de que existe where.
--en el semi-join se le agrega una condicion para tener un join mas filtrado
--ejemplo1: devuelve todas bolivianso que compraron libros.
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p inner join Libro l on p.persona_id = l.persona_id
where p.pais = 'Bolivia'

--ejemplo 2: muestra todo los clientes que ralizaron pedidos, que sus edades esten entre 30 y 50
use Tienda
select NonmbreCliente
from Cliente inner join Pedido on Cliente.Cliente_id = Pedido.Cliente_id
where edad between 30 and 50;
