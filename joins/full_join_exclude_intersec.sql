--FULL JOIN excluyendo la intersección: Este tipo de join devuelve todas las tuplas de las dos tablas
-- exepto loq ue tiene en comun
--ejemplo: muestra todos los libros sin dueños y personas sin libros
select *
from persona p full join Libro l on p.persona_id = l.persona_id
where p.persona_id is null or l.libro_id is null

-- ejemplo 2:mustra todas las mujeres sin libro y los libros sin dueño
select *
from persona p full join Libro l on p.persona_id = l.persona_id
where (p.persona_id is null or l.libro_id is null) and p.genero = 'f'