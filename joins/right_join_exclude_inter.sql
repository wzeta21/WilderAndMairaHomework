--RIGHT JOIN excluyendo la intersección: este tipo de join devuelve solo las tuplas de la
-- tabla derecha que tengan realcion con la tabla de la izquierda
--ejemplo:mostrar todas la personas que not tienen pais pero que que si compraron libros
select *
from Persona p right outer join Libro l on p.persona_id = l.persona_id
where p.pais is null

--ejemplo2: muestra todos los libros sin dueño
select p.*, l.*
from Persona p right outer join Libro l on p.persona_id = l.persona_id
where p.persona_id is null