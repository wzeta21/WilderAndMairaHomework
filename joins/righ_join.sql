--RIGH OUTER JOIN: Este tipo de join devuelve todo lo que tenga la tabla de la derecha mas 
--lo que tengan en comun entre las dos tablas
--ejemplo: Devuelve todas los libros mas sus dueños y los libros que no tengan dueños
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p right join Libro l on p.persona_id = l.persona_id

--ejemplo 2: muestra todos los pedidos y sus facturas
use Tienda
select Pedido.Pedido_id, Pedido.Fcatura 
from Cliente right join Pedido
on Cliente.Cliente_id = Pedido.Cliente_id
