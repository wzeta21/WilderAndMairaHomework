--FULL OUTER JOIN: Este tipo de join devuelve la union de las dos tablas, es decir devuelve todo lo 
--que tenga realcion en las dos tabals y los que no tengan realción
--ejemplo: devolver la union de las tablas persona y libro
select *
from persona p full join Libro l on p.persona_id = l.persona_id

--ejemplo 2:muestra todo las mujeres que tengan o no tenga libros
select *
from persona p full join Libro l on p.persona_id = l.persona_id
where p.genero = 'f'