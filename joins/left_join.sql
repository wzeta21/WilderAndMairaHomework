--LEFT OUTER JOIN: Este tipo de join devuelve todo lo que tenga la tabla de la izquierda mas lo
-- que tengan en comun entre las dos tablas

--ejemplo1:muestra todas las personas que compraron libro y los que no compraron libros.
select p.nombre, p.apellido, l.titulo, l.precio
from Persona p left outer join Libro l on p.persona_id = l.persona_id

--ejemplo 2: muestra todos los clientes con o sin pedidos ordenados pro nombre.
use Tienda 
SELECT Cliente.NonmbreCliente, Pedido.Pedido_id
FROM Cliente LEFT JOIN Pedido ON Cliente.Cliente_id=Pedido.Cliente_id
ORDER BY Cliente.NonmbreCliente;
