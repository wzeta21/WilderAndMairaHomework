--CROSS JOIN:Este tipo de join presenta el producto cartesiano de los registros de las dos tablas. 
--La tabla resultante tendrá todos los registros de la tabla izquierda combinados con cada uno de los 
--registros de la tabla derecha. El código SQL para realizar este producto cartesiano enuncia las 
--tablas que serán combinadas, pero no incluye algún predicado que filtre el resultado.

--ejemplo:mostrar el producto cartesiano de las tablas persona  lebro
select *
from persona p cross join Libro l

--ejemplo 2: muestra solo varones que en producto cartesiano
select *
from persona p cross join Libro l
where p.genero = 'm'